<?php

/**
 * @file
 * Simple Validation API documentation.
 */

/**
 * hook_simple_validation_rules_info()
 * Declare validation_rules
 *
 * @return
 *     An associative array of shortcodes, whose keys are internal shortcode names,
 *     which should be unique..
 *     Each value is an associative array describing the shortcode, with the
 *     following elements (all are optional except as noted):
 *   - title: (required) An administrative summary of what the shortcode does.
 *   - description: Additional administrative information about the shortcode's
 *     behavior, if needed for clarification.
 *   - settings callback: The name of a function that returns configuration form
 *     elements for the shortcode. TODO
 *   - default settings: An associative array containing default settings for
 *     the shortcode, to be applied when the shortcode has not been configured yet.
 *   - process callback: (required) The name the function that performs the
 *     actual shortcodeing.
 *   - tips callback: The name of a function that returns end-user-facing shortcode
 *     usage guidelines for the shortcode.
 *
 *   - TODO: wysiwyg or attributes? WYSIWYG callback: The name of a function that returns a FAPI array with
 *     configuration input for the shortcode
 *
 */
function hook_simple_validation_rules_info() {
  // Quote shortcode example
  $validation_rules['isMobile'] = array(
    'name' => t('isMobile'),
    'description' => t('Telphone validation.'),
    'methods callback' => 'simple_validation_rules_methods',
    'default_value' => 'true',
    'message' => 'This value can not empty',
    'js files' => array('jQuery.Validate.isMobile.js'),
  );

  return $validation_rules;
}

/**
 * hook_simple_validation_rules_info_alter()
 * Alter existing validation_rules
 *
 * @param $validation_rules
 *    An associative array of validation_rules
 *
 * @return
 *     An associative array of shortcodes, whose keys are internal shortcode names,
 *     which should be unique..
 *     Each value is an associative array describing the shortcode, with the
 *     following elements (all are optional except as noted):
 *   - title: (required) An administrative summary of what the shortcode does.
 *   - description: Additional administrative information about the shortcode's
 *     behavior, if needed for clarification.
 *   - settings callback: The name of a function that returns configuration form
 *     elements for the shortcode.
 *   - default settings: An associative array containing default settings for
 *     the shortcode, to be applied when the shortcode has not been configured yet.
 *   - process callback: (required) The name the function that performs the
 *     actual shortcodeing.
 *   - tips callback: The name of a function that returns end-user-facing shortcode
 *     usage guidelines for the shortcode.
 */
/**
 * @param $shortcodes
 */
function hook_simple_validation_rules_info_alter(&$validation_rules) {
  // Example to change the process callback for quotes.
  $validation_rules['isMobile']['methods callback'] = 'MYMODULE_simple_validation_rules';
}
