<?php

/**
 * @File
 * Administrative callbacks for PATH BREADCRUMBS module.
 */

/**
 * Page callback for module settings page.
 */
function simple_validation_admin_page() {

  // Load path breadcrumbs.
  $result = db_select('simple_validation', 'sv')
    ->fields('sv', array('sv_id', 'sv_form_id', 'sv_fields', 'sv_rules'))
    ->execute();

  $rows = array();
  foreach ($result as $rule) {
    $row    = array();
    $row[]  = $rule->sv_form_id;
    $row[]  = $rule->sv_fields;
		$row[]  = $rule->sv_rules;
    $row[]  = theme('item_list', array('items' => array(
        l(t('Edit'), 'admin/structure/simple-validation/edit/' . $rule->sv_id),
        l(t('Delete'), 'admin/structure/simple-validation/delete/' . $rule->sv_id),
      ),
    ));
    $rows[] = $row;
  }

  if ($rows) {
    // Build table if data exists.
    $headers = array(t('Form ID'), t('Validation Fields'), t('Rules'),  t('Actions'));
    $output = array(
      '#theme' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
    );
  }
  else {
    // Return message if no data exists.
    $output = t('There are no any validation created yet.');
  }

  return $output;
}
