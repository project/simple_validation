(function($) {

/**
 * Validate the forms.
 */
Drupal.behaviors.simple_validation = {
  attach: function(context, settings) {
    var countForm = 0;
    for(var n in settings.simple_validation){
      countForm++;
    }

    if(countForm > 0){
      $.each(settings.simple_validation, function(key,val){
        var formID = key;
        if(val.length > 0){
          var sub_rules = new Object();
          for(i=0;i<val.length;i++){
            var formField = val[i].sv_fields;
            var formRules = val[i].sv_rules;
            if(typeof val[i].sv_rules.required == "string" && val[i].sv_rules.required == "false"){            
              val[i].sv_rules.required = false;
            }else{
              val[i].sv_rules.required = true;
            }
            sub_rules[formField] = formRules;
          }

          var validator = $("input[name=form_id][value="+formID+"]").parents('form').bind("invalid-form.validate").validate({
            rules: sub_rules
          });
        }
      })
    }
  }
}

})(jQuery);
